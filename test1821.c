/*! \file  test1821.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date December 5, 2015, 4:05 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include "../include/LCD.h"
#include "../libDS1821.X/DS1821.h"

/* Configuration fuses                                                      */
#pragma config FNOSC = FRCPLL   /*!< Fast RC oscillator with PLL            */
#pragma config FWDTEN = OFF     /*!< Watchdog timer off                     */
#pragma config DMTEN = DISABLE  /*!< Deadman timer off                      */


//#pragma config OSCIOFNC = ON    /*!< OSC2 (RA3) is general purpose IO       */
//#pragma config IOL1WAY = OFF    /*!< Allow multiple PPS reconfigurations    */

/*! main - */
/*!
 *
 */
int main(void)
{
  int TempC, TempF;
  char szWork[32];
  int nClear;

  /* Set the clock to 70 MIPS so we know timing                 */
  CLKDIVbits.FRCDIV = 0;        /* Divide by 1 = 8MHz           */
  CLKDIVbits.PLLPRE = 0;        /* Divide by 2 = 4 MHz          */
  PLLFBD = 74;                  /* Multiply by 72 = 140         */
  CLKDIVbits.PLLPOST = 0;       /* Divide by 2 = 70             */

  Delay_ms(1000); /* Give it a breather               */
  LCDinit(); /* Initialize the LCD               */
  LCDclear();
  LCDputs("Read DS1821");
  Delay_ms(1000);

  // Set the DS1821 input port to digital
  ANSELAbits.ANSA2 = 0; // Make DS1821 port digital

  // Initialize the timers for the DS1821
  DS1821_Initialize(1, &PORTA, 2);

  LCDclear();
  LCDputs("Initialized");
  Delay_ms(1000);

  nClear = 1;
  while (1)
    {
      if (nClear)
        {
          LCDclear();
          nClear = 0;
        }
      // Read the temperature
      TempC = DS1821_ReadTemp(1);
      TempF = TempC * 9 / 5 + 32;
      sprintf(szWork, "%d C, %d F ", TempC, TempF );

      LCDhome();
      LCDputs(szWork);
    }
  while (1);
  return 0;

}
